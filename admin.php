<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Inicio</title>
    <style>
      input[type=submit]
      {
        background-color: #D6EAF8;
        padding: 4px 4px;
        border: outset #ABB2B9;
        cursor: pointer;
        font-size: 15px;
        font-weight: bold;
        box-shadow: 3px 4px 30px black;
      }
      .container
      {
        padding: 4px 4px;
        box-sizing: border-box;
        font-size: 20px;
        border:10px groove #140000;
        border-radius: 25px;
      }
    </style>
  </head>
  <body>
  <!--considera quitar el align-->
    <div class="container" align="center">
      <form action='tienda.php' method='post'><br>
        <input type='submit' value='Administrar Tiendas'>
      </form>
      <form action='producto.php' method='post'><br>
        <input type='submit' value='Administrar Productos'>
      </form>
      <form action='cliente.php' method='post'><br>
        <input type='submit' value='Administrar Clientes'>
      </form>
      <form action='venta.php' method='post'><br>
        <input type='submit' value='Administrar Ventas'>
      </form>
      <form action='detalle.php' method='post'><br>
        <input type='submit' value='Detalle de ventas'>
      </form>
    </div>
  </body>
</html>